#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import os
import subprocess
import re
from functools import partial

_open = partial(open, encoding="utf8")


content_stub_base = r"""\noindent
\hspace{2mm} К\makebox[0.5cm][l]{@[teamnumber]} \parbox[t][2cm]{3.5cm}{\raggedright @[teamname]} \hspace{0.9cm} В\makebox[0.5cm][l]{@[question_number]}"""

content_stub_no_name = r"""\noindent
\hspace{2mm} К\makebox[0.5cm][l]{@[teamnumber]} \hspace{4.4cm} В\makebox[0.5cm][l]{@[question_number]}"""

non_alpha = re.compile("[^a-zA-Z0-9А-Яа-яёЁ]")


def sanitize(team):
    team = non_alpha.sub("_", team)
    return team


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--stub")
    parser.add_argument("--team_names")
    parser.add_argument("--teams", type=int)
    parser.add_argument("--folder")
    parser.add_argument("--questions", type=int)
    parser.add_argument("--cleanup", action="store_true")
    parser.add_argument("--redo", action="store_true")
    parser.add_argument("--debug", action="store_true")
    args = parser.parse_args()

    with open(args.stub) as f:
        file_stub = f.read()

    if args.team_names:
        content_stub = content_stub_base
        with open(args.team_names) as f:
            team_names = [x.strip() for x in f.read().split("\n") if x.strip()]
    else:
        content_stub = content_stub_no_name
        team_names = [" " for _ in range(args.teams)]

    os.chdir(args.folder)
    if args.debug:
        sbp_kwargs = {}
    else:
        fnull = open(os.devnull, "w")
        sbp_kwargs = {"stdout": fnull, "stderr": subprocess.STDOUT}
    for tn, team in enumerate(team_names):
        print("processing {}".format(team))
        filename = "{}_{}.tex".format(str(tn + 1).zfill(3), sanitize(team))
        filename_pdf = "{}_{}.pdf".format(str(tn + 1).zfill(3), sanitize(team))
        if os.path.isfile(filename_pdf) and not args.redo:
            print("already exists, skipping")
            continue
        content_list = []
        for i in range(args.questions):
            content_list.append(
                content_stub.replace(
                    "@[teamname]", team
                ).replace(
                    "@[teamnumber]", str(tn + 1)
                ).replace(
                    "@[question_number]", str(i + 1)
                )
            )
        file_content = file_stub.replace(
            "@[content]", "\n\\clearpage\n".join(content_list)
        )
        with open(filename, "w") as f:
            f.write(file_content)
        subprocess.call(
            ["lualatex", filename], **sbp_kwargs
        )
    if args.cleanup:
        for filename in os.listdir(os.getcwd()):
            if filename.endswith((".log", ".aux", ".tex")):
                os.remove(filename)



if __name__ == "__main__":
    main()
