from reportlab.pdfgen import canvas
from reportlab.lib.units import mm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.platypus import SimpleDocTemplate, Frame, Paragraph
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib import colors
from reportlab.lib.pagesizes import A4

font_path = "/Users/pecheny/Library/Fonts/Open_Sans/static/OpenSans/OpenSans-Light.ttf"
font_name = "Open Sans Light"

pdfmetrics.registerFont(TTFont(font_name, font_path))


def coord(x, y, unit=mm):
    x, y = x * unit, A4[1] - y * unit
    return x, y


c = canvas.Canvas("hello.pdf", pagesize=A4)


c.setFont("Open Sans Light", 8)
c.drawString(*coord(2, 4), text="К1")
c.drawString(*coord(62, 4), text="В1")
c.setStrokeColorRGB(0, 0, 0)  # Set the stroke color to black
c.setLineWidth(0.2)
c.rect(*coord(0, 38), 70 * mm, 38 * mm, fill=0, stroke=1)
content = "Благотворительная организация «Водохлёбус»"
style = ParagraphStyle(
    name="Normal",
    fontName="Open Sans Light",
    fontSize=8,
    leading=10,
    textColor=colors.black,
    spaceBefore=0,
    spaceAfter=0,
    borderPadding=0,
    valign=1,
)
paragraph = Paragraph(content, style)
w, h = paragraph.wrapOn(c, 50 * mm, 10 * mm)
paragraph.drawOn(c, *coord(8, 1.2 + h/mm))
c.showPage()
c.save()