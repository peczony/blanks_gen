#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import os
import subprocess
from functools import partial

_open = partial(open, encoding="utf8")

content_stub_base = r"""\noindent
\hspace{2mm} К\makebox[0.5cm][l]{@[teamnumber]} \parbox[t][2cm]{3.5cm}{\raggedright @[teamname]} \hspace{0.9cm} В\makebox[0.5cm][l]{@[question_number]}"""

content_stub_no_name = r"""\noindent
\hspace{2mm} К\makebox[0.5cm][l]{@[teamnumber]} \hspace{4.4cm} В\makebox[0.5cm][l]{@[question_number]}"""


def sanitize(team):
    team = team.replace(" ", "_")
    team = team.replace("~", "_")
    return team


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--stub")
    parser.add_argument("--team_names")
    parser.add_argument("--teams", type=int)
    parser.add_argument("--folder")
    parser.add_argument("--batch_len", type=int, default=24)
    parser.add_argument("--questions", type=int)
    parser.add_argument("--cleanup", action="store_true")
    parser.add_argument("--redo", action="store_true")
    parser.add_argument("--debug", action="store_true")
    args = parser.parse_args()

    with _open(args.stub) as f:
        file_stub = f.read()

    if args.team_names:
        content_stub = content_stub_base
        with _open(args.team_names) as f:
            team_names = [x.strip() for x in f.read().split("\n") if x.strip()]
    else:
        content_stub = content_stub_no_name
        team_names = [" " for _ in range(args.teams)]

    os.chdir(args.folder)
    if args.debug:
        sbp_kwargs = {}
    else:
        fnull = _open(os.devnull, "w")
        sbp_kwargs = {"stdout": fnull, "stderr": subprocess.STDOUT}
    batches = []
    teams_en = list(enumerate(team_names))
    while len(teams_en):
        batch, teams_en = (
            teams_en[: args.batch_len],
            teams_en[args.batch_len :],
        )
        batches.append(batch)
    for bn, batch in enumerate(batches):
        print("processing batch {}".format(bn + 1))
        bn_zfilled = str(bn + 1).zfill(3)
        filename = "{}_batch{}.tex".format(
            os.path.splitext(args.stub)[0], bn_zfilled
        )
        filename_pdf = "{}_batch{}.pdf".format(
            os.path.splitext(args.stub)[0], bn_zfilled
        )
        if os.path.isfile(filename_pdf) and not args.redo:
            print("already exists, skipping")
            continue
        content_list = []
        for i in range(args.questions):
            for tup in batch:
                content_list.append(
                    content_stub.replace("@[teamname]", tup[1])
                    .replace("@[teamnumber]", str(tup[0] + 1))
                    .replace("@[question_number]", str(i + 1))
                )
        file_content = file_stub.replace(
            "@[content]", "\n\\clearpage\n".join(content_list)
        )
        with _open(filename, "w") as f:
            f.write(file_content)
        subprocess.call(["lualatex", filename], **sbp_kwargs)
    if args.cleanup:
        for filename in os.listdir(os.getcwd()):
            if filename.endswith((".log", ".aux", ".tex")):
                os.remove(filename)


if __name__ == "__main__":
    main()
